// Refcount implements a refcounted cache.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package refcount

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

// CreateFunc is a function that can be called to create a missing value for the cache.
type CreateFunc func() (value interface{}, err error)

// DeleteFunc is a function that is called when a value is deleted.
type DeleteFunc func(value interface{})

// Cache provides a key-value cache with ref count. Values will be deleted from the cache when their ref count drops to zero.
//
// Cache is safe for concurrent use by multiple goroutines without additional locking or coordination. The zero Cache is empty and ready for use. A Cache must not be copied after first use.
type Cache struct {
	// Delete optionally specifies a function that will be called with any
	// value that has been deleted from the cache. It should not be changed
	// after the Cache has been used.
	Delete DeleteFunc
	// Delay specifies an optional delay before a value with zero ref count
	// is deleted from the Cache. It should not be changed after the Cache
	// has been used. The behaviour for negative values of Delay is
	// undefined.
	Delay time.Duration
	m     sync.Map // The underlying map
}

// element represents an element in the cache.
type element struct {
	sync.Mutex
	Value      interface{} // The value
	Key        interface{} // The key
	C          *Cache      // The parent cache
	refCount   int         // The current ref count
	deleted    bool        // Has this element been deleted?
	hasWorker  bool        // Is a deletion worker active?
	expiryTime time.Time   // The scheduled deletion time
}

// Reference is a reference to a value.
type Reference struct {
	e *element
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// execCreateFunc executes the given CreateFunc, recovering from any panics.
func execCreateFunc(f CreateFunc) (value interface{}, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("Panic in CreateFunc: %v", e)
		}
	}()
	value, err = f()
	return
}

// referenceFinalizer is the finaliser for a reference. This decrements the ref count on the referenced element.
func referenceFinalizer(r *Reference) {
	r.e.DecRef()
}

// deleteElement deletes the element e from its parent cache. Assumes that the caller owns a lock on the element. On return the lock will have been released.
func deleteElement(e *element) {
	// Mark us as deleted and remove us from the cache
	e.deleted = true
	e.C.removeElement(e.Key)
	// Release the lock
	e.Unlock()
	// If a delete function is provided, call it on the value
	if e.C.Delete != nil {
		e.C.Delete(e.Value)
	}
}

// deleteWorker schedules the element e for deletion. Intended to be run in its own go routine.
func deleteWorker(e *element, expiryTime time.Time) {
	t := time.NewTimer(time.Until(expiryTime))
	for {
		// Wait for the timer to fire
		<-t.C
		// Acquire a lock
		e.Lock()
		// Is there anything to do?
		expiryTime = e.expiryTime
		if e.deleted || expiryTime.IsZero() {
			e.hasWorker = false
			e.Unlock()
			return
		}
		// Is it time to delete the element?
		now := time.Now()
		if !now.Before(expiryTime) {
			deleteElement(e) // Note that this function releases the lock
			return
		}
		// We need to wait a little longer
		e.Unlock()
		t.Reset(expiryTime.Sub(now))
	}
}

/////////////////////////////////////////////////////////////////////////
// Cache functions
/////////////////////////////////////////////////////////////////////////

// removeElement removes the element for the given key (if any). This performs no sanity checks, and no additional processing; it simply removes the element.
func (c *Cache) removeElement(key interface{}) {
	c.m.Delete(key)
}

// loadElement returns the element for the given key. The element is returned locked: it is the caller's responsibility to ensure that it is unlocked. If no element is assigned, or if the existing element is no longer valid, then nil is returned. The second return value is true iff a valid element was found in the cache.
func (c *Cache) loadElement(key interface{}) (*element, bool) {
	// Recover the element for this key
	x, ok := c.m.Load(key)
	if !ok {
		return nil, false
	}
	e := x.(*element)
	// Acquire a lock
	e.Lock()
	// Is the element still valid?
	if e.deleted {
		e.Unlock()
		return nil, false
	}
	return e, true
}

// Load returns a reference to the value stored in the cache for a key, or nil if no value is present. The second return value is true iff the value was found in the cache. Creation of a reference will increment the ref count for the stored value. A finaliser is installed (via runtime.SertFinalizer) on the returned reference that will automatically decrement the ref count when the reference is garbage collected. The user must not clear this finalizer, otherwise the ref count will never drop to zero and the value will remain in the cache.
func (c *Cache) Load(key interface{}) (*Reference, bool) {
	// Sanity check
	if c == nil {
		return nil, false
	}
	// Recover the element for this key
	e, ok := c.loadElement(key)
	if !ok {
		return nil, false
	}
	defer e.Unlock()
	// Return a reference
	return e.createReference(), true
}

// loadOrCreateElement returns the element for the given key. If no element is assigned, or if the existing element is no longer valid, then a new (valid) element will be assigned. The element is returned locked: it is the caller's responsibility to ensure that it is unlocked. The second return value is true iff the element was loaded.
func (c *Cache) loadOrCreateElement(key interface{}) (*element, bool) {
	// Create a new element for storing
	e := &element{
		Key: key,
		C:   c,
	}
	e.Lock()
	// Load or store the element for this key
	x, ok := c.m.LoadOrStore(key, e)
	if ok {
		e.Unlock()
		e = x.(*element)
		e.Lock()
	}
	// Is the element still valid?
	if e.deleted {
		e.Unlock()
		return c.loadOrCreateElement(key)
	}
	return e, ok
}

// LoadOrStore returns a reference to the existing value for the key if present. Otherwise, it stores the given value and returns a reference to that value. The second return value is true iff the value was loaded. See Load for remarks on the lifetime of the reference.
func (c *Cache) LoadOrStore(key interface{}, value interface{}) (*Reference, bool) {
	// Load or create the element for this key
	e, ok := c.loadOrCreateElement(key)
	defer e.Unlock()
	// If necessary set the value
	if !ok {
		e.Value = value
	}
	// Return a reference
	return e.createReference(), ok
}

// LoadOrCreate returns a reference to the existing value for the key if present. Otherwise, it stores the value created by the function f and returns a reference to that value. If f returns an error, then no value will be stored and the error will be returned. Note: If you need to know whether f was called -- thus recovering the functionality of the boolean return value in LoadOrStore -- you can do this via a closure in f. See Load for remarks on the lifetime of the reference.
func (c *Cache) LoadOrCreate(key interface{}, f CreateFunc) (*Reference, error) {
	// Load or create the element for this key
	e, ok := c.loadOrCreateElement(key)
	defer e.Unlock()
	// If necessary set the value
	if !ok {
		value, err := execCreateFunc(f)
		if err != nil {
			e.deleted = true
			c.removeElement(key)
			return nil, err
		}
		e.Value = value
	}
	// Return a reference
	return e.createReference(), nil
}

// Range calls f sequentially for each key and value present in the cache. If f returns false, range stops the iteration. See Load for remarks on the lifetime of the reference.
func (c *Cache) Range(f func(key interface{}, r *Reference) bool) {
	// Sanity check
	if c == nil {
		return
	}
	// Perform the range
	c.m.Range(func(key interface{}, x interface{}) bool {
		e := x.(*element)
		var ok bool
		var r *Reference
		e.Lock()
		if !e.deleted {
			ok = true
			r = e.createReference()
		}
		e.Unlock()
		if ok {
			ok = f(key, r)
			runtime.KeepAlive(r) // Ensure that the reference survives the call
			return ok
		}
		return true
	})
}

/////////////////////////////////////////////////////////////////////////
// element functions
/////////////////////////////////////////////////////////////////////////

// createReference returns a reference to this element. This will increment the ref count of e. The returned reference will have a finaliser installed that will decrement the ref count of e when the reference is garbage collected. Assumes that the caller owns a lock on the element.
func (e *element) createReference() *Reference {
	// If necessary, clear the expiry time
	if e.refCount == 0 {
		e.expiryTime = time.Time{}
	}
	// Increment the ref count
	e.refCount++
	// Create a reference
	r := &Reference{e: e}
	runtime.SetFinalizer(r, referenceFinalizer)
	return r
}

// RefCount returns the ref count for this element.
func (e *element) RefCount() int {
	// Acquire a lock
	e.Lock()
	defer e.Unlock()
	// Return the ref count
	return e.refCount
}

// DecRef decrements the ref count for this element.
func (e *element) DecRef() {
	// Acquire a lock
	e.Lock()
	// Decrement the ref count
	e.refCount--
	// If the new ref count is non-zero, there's nothing more to do
	if e.refCount != 0 {
		e.Unlock()
		return
	}
	// Does the cache have a delay before deleting elements?
	d := e.C.Delay
	if d == 0 {
		deleteElement(e) // Note that this function releases the lock
		return
	}
	// Set the expiry time
	e.expiryTime = time.Now().Add(d)
	// If necessary, launch a background worker to delete this element
	if !e.hasWorker {
		e.hasWorker = true
		go deleteWorker(e, e.expiryTime)
	}
	// Release the lock
	e.Unlock()
}

/////////////////////////////////////////////////////////////////////////
// Reference functions
/////////////////////////////////////////////////////////////////////////

// RefCount returns the ref count for the referenced value.
func (r *Reference) RefCount() int {
	if r == nil || r.e == nil {
		return 1
	}
	return r.e.RefCount()
}

// Value returns the value referenced by r. The caller must take care to keep the reference r in scope whilst the value is in use. This can be achieved by runtime.KeepAlive(r).
func (r *Reference) Value() interface{} {
	if r == nil || r.e == nil {
		return nil
	}
	return r.e.Value
}
